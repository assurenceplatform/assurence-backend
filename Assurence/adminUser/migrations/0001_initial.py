# Generated by Django 4.0.2 on 2022-04-08 13:02

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Admin',
            fields=[
                ('id', models.CharField(default=uuid.UUID('37682171-609d-4ad8-b433-f03517d0c847'), max_length=120, primary_key=True, serialize=False, unique=True)),
                ('telephone', models.CharField(blank=True, default='', max_length=15, null=True)),
                ('courier', models.CharField(blank=True, default='', max_length=150, null=True)),
                ('email', models.CharField(blank=True, default='', max_length=150, null=True)),
                ('user', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
