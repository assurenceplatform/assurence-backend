# Generated by Django 4.0.2 on 2022-04-11 22:23

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('adminUser', '0009_alter_admin_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='admin',
            name='id',
            field=models.CharField(default=uuid.UUID('0dc59280-8098-4ea1-81a9-8973310251e1'), max_length=120, primary_key=True, serialize=False, unique=True),
        ),
    ]
