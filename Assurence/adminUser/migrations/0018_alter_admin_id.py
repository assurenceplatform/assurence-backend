# Generated by Django 4.0.2 on 2022-04-15 10:05

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('adminUser', '0017_alter_admin_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='admin',
            name='id',
            field=models.CharField(default=uuid.UUID('e9554980-6365-4c81-afe2-93643c4e843f'), max_length=120, primary_key=True, serialize=False, unique=True),
        ),
    ]
