# Generated by Django 4.0.2 on 2022-04-15 10:13

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('adminUser', '0019_alter_admin_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='admin',
            name='id',
            field=models.CharField(default=uuid.UUID('f3d4e354-9666-4983-9575-c19f060efdc7'), max_length=120, primary_key=True, serialize=False, unique=True),
        ),
    ]
