from django.db import models
from user.models import UserAccount
import uuid


class Admin(models.Model):
    id = models.CharField(max_length=120, default=uuid.uuid4(), primary_key=True, unique=True)
    user = models.OneToOneField(UserAccount, on_delete=models.CASCADE, blank=True, null=True)
    telephone = models.CharField(max_length=15, blank=True, null=True , default="")
    nom =  models.CharField(max_length= 25 ,blank=True, null=True , default="")
    prenom =  models.CharField(max_length= 25 ,blank=True, null=True , default="")
    courier = models.CharField(max_length=150, blank=True, null=True ,  default="")
    email = models.CharField(max_length=150, blank=True, null=True ,  default="")

    def get_full_name(self):
        return str(self.id)

    def __str__(self):
        return self.nom +" " +self.prenom +"     "+str(self.id)