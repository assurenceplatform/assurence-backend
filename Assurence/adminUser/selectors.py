'''
from django.db.models.query import QuerySet
from .models import Admin


def get_admins() -> QuerySet[Admin]:
    qs = Admin.objects.all()
    return qs'''

from django.db.models.query import QuerySet
from django.apps import apps

Admin = apps.get_model('adminUser', 'Admin')
UserAccount = apps.get_model('user', 'UserAccount')


def verify_admin(id) -> bool:
    print("verif  admin", id)
    try:
        admin = Admin.objects.get(id=id)
    except Admin.DoesNotExist:
        return False
    return True


def get_all() -> QuerySet[Admin]:
    try:
        admins = Admin.objects.all()
    except Admin.DoesNotExist:
        admins = None
    return admins


def get_Admin(id) -> Admin:
    try:
        admin = Admin.objects.get(id=id)
    except Admin.DoesNotExist:
        admin = None
    return admin


def get_Admin_By_Email(email) -> Admin:
    try:
        user= UserAccount.objects.get(email=email)
        admin = Admin.objects.get(user=user)
    except Admin.DoesNotExist or UserAccount.DoesNotExist:
        return {"error": "there is no admin by that email "}
    return admin
