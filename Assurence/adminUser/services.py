"""
    services is a boxe (I named it boxe because it is considered like a buisiness logic holder )
    services  will be focusing on buisiness logic on post  update methods
    ==> buisiness logic will  be here (:
"""

from django.apps import apps
from django.core.mail import send_mail
from uuid import uuid4
from prospect.selectors import get_Prospect

Admin = apps.get_model('adminUser', 'Admin')
Prospect = apps.get_model('prospect', 'Prospect')
UserAccount = apps.get_model('user', 'UserAccount')


def verif_user_email(email) -> bool:
    if UserAccount.objects.filter(email=email).exists():
        return True
    return False


def create_user(data) -> UserAccount:
    if verif_user_email(data["email"]):
        return None
    user = UserAccount.objects.create(email=data['email'], name=data['name'], password=data['password'])
    if isinstance(user, UserAccount):
        return user
    return None


def create_Admin(data):
    prospect = get_Prospect(data["prospect"])
    if isinstance(prospect, Prospect):
        user = Sign_up_user(data['user'])
        if isinstance(user, UserAccount):
            client = Admin(prospect=prospect, email=data['user']['email'], user=user)
            client.save()
            return client
        return {"erreur", "cannot create user"}
    return {"erreur", "prospect n'existe pas"}


def send_mail_admin_signup(password, email) -> bool:
    send_mail("Validation de compte Habitat",
              f'Suite a la creation de votre compte nous fournissons un mot de passe !! '
              f'\n\n email : {email} '
              f'\n mdp : {password} '
              f'\n prière de modifier votre mot de passe dés que possible  ci joint vos coordonner ',
              "jassem.elwaar@gmail.com", [email],
              fail_silently=False)
    return True


def Sign_up_user(data):
    name = data['name']
    email = data['email']
    password = str(uuid4())

    if password:
        if UserAccount.objects.filter(email=email).exists():
            return None
        else:
            if len(password) < 6:
                return None
            else:
                user = UserAccount.objects.create_superuser(email, name, password)
                send_mail_admin_signup(password, email)
                if isinstance(user, UserAccount):
                    return user
                return None
    else:
        return None
