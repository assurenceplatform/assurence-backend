from django.urls import path
from .views import *

urlpatterns = [
    # les urls de admin
    # post
    path('create/', create_admin, name="create_admin"),
    path('delete/', delete_admin, name="delete_admin"),
    path('modifier_email/', modifier_admin_email, name="delete_admin"),
    path('modifier_courier/', modifier_admin_courier, name="delete_admin"),
    path('modifier_telephone/', modifier_admin_telephone, name="delete_admin"),


    # get
    path('get_admins/', get_admins, name="get_all_admins"),
    path('get_admin/', get_admin, name="get_one_admin"),
    path('get_admin_by_email/', get_admin_email, name="get_admin_by_email"),
    path('get_admin_data/', get_admin_data, name="get_admin_data"),
    # path('/update/<str:id>', update_admin, name="update_admin"),
]
