from django.shortcuts import render
from rest_framework.permissions import AllowAny
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from prospect.models import Prospect
from .services import *
from .selectors import *
from .serializer import *
from rest_framework import status
from django.contrib.auth import get_user_model
from rest_framework.response import Response
from rest_framework.permissions import IsAdminUser ,BasePermission



UserAccount = get_user_model()



@api_view(['DELETE'])
@permission_classes([AllowAny])
def delete_admin(request):
    data = request.data
    id = data['id']
    try:
        admin = Admin.objects.get(id=id)
    except Prospect.DoesNotExist:
        admin = None

    if admin:
        admin.delete()
        return Response({"Success": "delete has been done successefuly"})
    return Response({"Error": "Admin not Found"})


@api_view(['GET'])
@permission_classes([AllowAny])
def get_admins(request):
    admins = get_all()
    if admins:
        print(admins)
        serialiszer = AdminSerializers(admins, many=True)
        return Response(serialiszer.data)
    return Response({"error": "there is no admins in the Database"})


@api_view(['POST'])
@permission_classes([AllowAny])
def get_admin(request):
    data = request.data
    print(data)
    id = data['id']
    admin = get_Admin(id)
    if admin:
        serialiszer = AdminSerializers(admin, many=False)
        return Response(serialiszer.data)
    return {"error": "there is no admins in the Database"}


@api_view(['POST'])
@permission_classes([AllowAny])
def create_admin(request):
    data = request.data
    admin = create_Admin(data)
    if isinstance(admin, Admin):
        serializer = AdminSerializers(admin, many=False)
        return Response(serializer.data)
    return Response(admin)


@api_view(['GET'])
@permission_classes([AllowAny])
def get_admin_email(request):
    data = request.data
    email = data["email"]
    admin = get_Admin_By_Email(email)
    if isinstance(admin, Admin):
        serializer = AdminSerializers(admin, many=False)
        return Response(serializer.data)
    return Response(admin)


@api_view(['POST'])
@permission_classes([AllowAny])
def get_admin_data(request):
    data = request.data
    print(data)
    email = data['email']
    admin = get_Admin_By_Email(email)
    if admin:
        return Response(
            {"id": admin.id, "nom": admin.nom, "prenom": admin.prenom, "email": admin.user.email,
             "courier": admin.courier,
             "numero_telephone": admin.telephone})
    return Response({"error": "Admin n'existe pas ! "}, status=status.HTTP_404_NOT_FOUND)


@api_view(['POST'])
@permission_classes([AllowAny])
def modifier_admin_telephone(request):
    data = request.data
    id = data['id']
    telephone = data['teleph']
    admin = get_Admin(id)
    if admin:
        admin.telephone = telephone
        admin.save()
        ser = AdminSerializers(admin, many=False)
        return Response(ser.data, status=status.HTTP_200_OK)
    return Response({"error": "Admin n'existe pas ! "}, status=status.HTTP_404_NOT_FOUND)


@api_view(['POST'])
@permission_classes([AllowAny])
def modifier_admin_email(request):
    data = request.data
    id = data['id']
    email = data['email']
    admin = get_Admin(id)
    if admin:
        admin.email = email
        admin.save()
        ser = AdminSerializers(admin, many=False)
        return Response(ser.data, status=status.HTTP_200_OK)
    return Response({"error": "Admin n'existe pas ! "}, status=status.HTTP_404_NOT_FOUND)


@api_view(['POST'])
@permission_classes([AllowAny])
def modifier_admin_courier(request):
    data = request.data
    id = data['id']
    courier = data['courier']
    admin = get_Admin(id)
    if admin:
        admin.courier = courier
        admin.save()
        ser = AdminSerializers(admin, many=False)
        return Response(ser.data, status=status.HTTP_200_OK)
    return Response({"error": "Admin n'existe pas ! "}, status=status.HTTP_404_NOT_FOUND)


