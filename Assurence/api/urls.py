from django.contrib import admin
from django.urls import path, include, re_path
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
                  path('admin/', admin.site.urls),
                  path('prospect/', include('prospect.urls')),
                  path('adminUser/', include('adminUser.urls')),
                  path('devis/', include('devis.urls')),
                  path('plainte/', include('plaintes.urls')),
                  path('client/', include('client.urls')),
                  path('sinistre/', include('sinistre.urls')),
                  path('contrat/', include('contrat.urls')),
                  path('notification/', include('notification.urls')),
                  path('api-auth/', include('rest_framework.urls')),
                  path('auth/', include('djoser.urls')),
                  path('auth/', include('djoser.urls.jwt')),
                  path('email/', include('emailSender.urls')),
                  path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
                  path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
