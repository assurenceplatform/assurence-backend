# Generated by Django 4.0.2 on 2022-03-22 17:46

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0006_alter_client_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='id',
            field=models.CharField(default=uuid.UUID('92ccb2da-f616-4645-9f98-0cae5383f73d'), max_length=120, primary_key=True, serialize=False, unique=True),
        ),
    ]
