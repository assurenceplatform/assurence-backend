# Generated by Django 4.0.2 on 2022-03-31 15:45

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0027_alter_client_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='id',
            field=models.CharField(default=uuid.UUID('43481623-47d8-42c3-864b-ceeb82c95ad3'), max_length=120, primary_key=True, serialize=False, unique=True),
        ),
    ]
