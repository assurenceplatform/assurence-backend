# Generated by Django 4.0.2 on 2022-04-08 13:02

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0029_alter_client_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='id',
            field=models.CharField(default=uuid.UUID('b3f16099-866c-4b3a-bbaf-e7ff6eeb0d24'), max_length=120, primary_key=True, serialize=False, unique=True),
        ),
    ]
