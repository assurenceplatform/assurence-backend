# Generated by Django 4.0.2 on 2022-04-14 13:56

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0044_alter_client_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='id',
            field=models.CharField(default=uuid.UUID('b383d032-8e90-4ae5-9a5c-0f2d2eccd09e'), max_length=120, primary_key=True, serialize=False, unique=True),
        ),
    ]
