# Generated by Django 4.0.2 on 2022-04-15 10:13

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0048_alter_client_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='id',
            field=models.CharField(default=uuid.UUID('ab8539d6-dfc6-4716-a9b1-a6466f5fd105'), max_length=120, primary_key=True, serialize=False, unique=True),
        ),
    ]
