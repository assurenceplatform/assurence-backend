'''
from django.db.models.query import QuerySet
from .models import Client


def get_clients() -> QuerySet[Client]:
    qs = Client.objects.all()
    return qs'''

from django.db.models.query import QuerySet
from django.apps import apps

Client = apps.get_model('client', 'Client')
UserAccount = apps.get_model('user', 'UserAccount')


def verify_client(id) -> bool:
    print("verif  client", id)
    try:
        client = Client.objects.get(id=id)
    except Client.DoesNotExist:
        return False
    return True


def get_all() -> QuerySet[Client]:
    try:
        clients = Client.objects.all()
    except Client.DoesNotExist:
        clients = None
    return clients


def get_Client(id) -> Client:
    try:
        client = Client.objects.get(id=id)
    except Client.DoesNotExist:
        client = None
    return client


def get_Client_By_Email(email) -> Client:
    try:
        user= UserAccount.objects.get(email=email)
        client = Client.objects.get(user=  user)
    except Client.DoesNotExist or UserAccount.DoesNotExist:
        return {"error": "there is no client by that email "}
    return client
