from django.urls import path
from .views import *

urlpatterns = [
    # les urls de client
    # post
    path('create/', create_client, name="create_client"),
    path('delete/', delete_client, name="delete_client"),
    path('modifier_email/', modifier_client_email, name="delete_client"),
    path('modifier_courier/', modifier_client_courier, name="delete_client"),
    path('modifier_telephone/', modifier_client_telephone, name="delete_client"),


    # get
    path('get_clients/', get_clients, name="get_all_clients"),
    path('get_client/', get_client, name="get_one_client"),
    path('get_client_by_email/', get_client_email, name="get_client_by_email"),
    path('get_client_data/', get_client_data, name="get_client_data"),
    # path('/update/<str:id>', update_client, name="update_client"),
]
