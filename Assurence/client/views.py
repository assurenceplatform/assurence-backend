from django.shortcuts import render
from rest_framework.permissions import AllowAny
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from prospect.models import Prospect
from .services import *
from .selectors import *
from .serializer import *
from rest_framework import status
from prospect.selectors import get_Prospect
from django.contrib.auth import get_user_model

UserAccount = get_user_model()
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import permissions
from uuid import uuid4


# Create your views here.

@api_view(['DELETE'])
@permission_classes([AllowAny])
def delete_client(request):
    data = request.data
    id = data['id']
    try:
        client = Client.objects.get(id=id)
    except Prospect.DoesNotExist:
        client = None

    if client:
        client.delete()
        return Response({"Success": "delete has been done successefuly"})
    return Response({"Error": "Client not Found"})


@api_view(['GET'])
@permission_classes([AllowAny])
def get_clients(request):
    clients = get_all()
    if clients:
        print(clients)
        serialiszer = ClientSerializers(clients, many=True)
        return Response(serialiszer.data)
    return Response({"error": "there is no clients in the Database"})


@api_view(['POST'])
@permission_classes([AllowAny])
def get_client(request):
    data = request.data
    print(data)
    id = data['id']
    client = get_Client(id)
    if client:
        serialiszer = ClientSerializers(client, many=False)
        return Response(serialiszer.data)
    return {"error": "there is no clients in the Database"}


@api_view(['POST'])
@permission_classes([AllowAny])
def create_client(request):
    data = request.data
    client = create_Client(data)
    if isinstance(client, Client):
        serializer = ClientSerializers(client, many=False)
        return Response(serializer.data)
    return Response(client)


@api_view(['GET'])
@permission_classes([AllowAny])
def get_client_email(request):
    data = request.data
    email = data["email"]
    client = get_Client_By_Email(email)
    if isinstance(client, Client):
        serializer = ClientSerializers(client, many=False)
        return Response(serializer.data)
    return Response(client)


@api_view(['POST'])
@permission_classes([AllowAny])
def get_client_data(request):
    data = request.data
    print(data)
    email = data['email']
    client = get_Client_By_Email(email)
    if client:
        return Response(
            {"id": client.id, "nom": client.prospect.nom, "prenom": client.prospect.prenom, "email": client.user.email,
             "courier": client.courier,
             "numero_telephone": client.telephone})
    return Response({"error": "Client n'existe pas ! "}, status=status.HTTP_404_NOT_FOUND)


@api_view(['POST'])
@permission_classes([AllowAny])
def modifier_client_telephone(request):
    data = request.data
    id = data['id']
    telephone = data['teleph']
    client = get_Client(id)
    if client:
        client.telephone = telephone
        client.save()
        ser = ClientSerializers(client, many=False)
        return Response(ser.data, status=status.HTTP_200_OK)
    return Response({"error": "Client n'existe pas ! "}, status=status.HTTP_404_NOT_FOUND)


@api_view(['POST'])
@permission_classes([AllowAny])
def modifier_client_email(request):
    data = request.data
    id = data['id']
    email = data['email']
    client = get_Client(id)
    if client:
        client.email = email
        client.save()
        ser = ClientSerializers(client, many=False)
        return Response(ser.data, status=status.HTTP_200_OK)
    return Response({"error": "Client n'existe pas ! "}, status=status.HTTP_404_NOT_FOUND)


@api_view(['POST'])
@permission_classes([AllowAny])
def modifier_client_courier(request):
    data = request.data
    id = data['id']
    courier = data['courier']
    client = get_Client(id)
    if client:
        client.courier = courier
        client.save()
        ser = ClientSerializers(client, many=False)
        return Response(ser.data, status=status.HTTP_200_OK)
    return Response({"error": "Client n'existe pas ! "}, status=status.HTTP_404_NOT_FOUND)






