from pytest_factoryboy import register
import pytest

from user.tests.Userfactories import UserAccountFactory

register(UserAccountFactory)


@pytest.fixture
def new_user(db, user_account_factory):
    user = user_account_factory.create()
    return user
