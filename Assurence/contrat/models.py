from django.db import models
from devis.models import Devis_Maison, Devis_Appartement, Devis_Immeuble
from client.models import Client
import uuid


class Contrat(models.Model):
    """
        Contrat est une classe qui serve a transformer un devis en une contrat le temps le paiment de devis sera mis en place
        un contrat et a la base devis payé et  tous type de devis se traduit en une contrat
    """
    Type_devis = (("Maison", "Maison"), ("Appartement", "Appartement"),
                  ("Immeuble", "Immeuble"))
    status_contrat = (("en cours", "en cours"), ("besoin de paiement", "besoin de paiement"),
                      ("achevée", "achevée"))
    id = models.CharField(default=uuid.uuid4, unique=True, primary_key=True, max_length=20, editable=False)
    paiment = models.BooleanField(default=True)
    contrat_type = models.CharField(max_length=255, choices=Type_devis)
    contrat_body = models.CharField(max_length=25, null=True, blank=True)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    status = models.CharField(max_length=50, null=True, blank=True, default='en cours', choices=status_contrat)
    adresse = models.CharField(max_length=150, blank=True, null=True)
    id_devis = models.CharField(max_length=150, default=None, null=True, blank=True)
    type_contrat = models.CharField(max_length=30, default=None, null=True, blank=True)
    tarification = models.CharField(max_length=15, default="00.00", null=True, blank=True)
    date_contrat = models.DateField(auto_now_add=True)
    contrat_image = models.FileField(upload_to='static/contrat/origin/%Y/%m/%d/', default=None, blank=True, null=True)

    def __str__(self):
        return str(self.id) + " " + self.contrat_type
