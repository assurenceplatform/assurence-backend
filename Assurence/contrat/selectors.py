from .models import *
from django.apps import apps
from rest_framework.response import Response

Client = apps.get_model('client', 'Client')
from .serializer import ContratSerializers


def verif_client(id) -> bool:
    try:
        client = Client.objects.get(id=id)
        return True
    except Client.DoesNotExist:
        return False


def get_all_client_contrat(data):
    print("tis is ", data)
    id = data['id']
    if verif_client(id):
        try:
            contrats_client = Contrat.objects.filter(client=id)
        except Contrat.DoesNotExist:
            return {"error": "Ce client n'a pas des contrats"}
        serializer = ContratSerializers(contrats_client, many=True)
        return serializer.data
    return {"error": "le client n'existe pas "}


def get_all_contrats():
    try:
        contrats = Contrat.objects.all()
    except Contrat.DoesNotExist:
        return {"error": "there is no contrats"}
    serialiser = ContratSerializers(contrats, many=True)
    print(serialiser.data)
    return serialiser.data
