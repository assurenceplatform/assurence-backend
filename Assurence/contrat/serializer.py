from rest_framework.serializers import ModelSerializer
from .models import *


class ContratSerializers(ModelSerializer):
    class Meta:
        model = Contrat
        fields = '__all__'
