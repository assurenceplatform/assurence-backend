from django.urls import path
from .views import *

urlpatterns = [
    #client requests
    path('get_all/', client_all_contrat, name="get_all_client_contrat"),
    #admin requests
    path('get_all_contrat/', get_all_contrat, name="get_all_contrat"),
]

