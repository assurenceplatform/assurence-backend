from rest_framework.permissions import AllowAny
from rest_framework.decorators import api_view, permission_classes
from django.views import View
from rest_framework.response import Response
from .models import *
from devis.models import Devis_Immeuble, Devis_Appartement, Devis_Maison
from .selectors import *
import uuid
from .serializer import *


# Create your views here.


@api_view(["GET"])
@permission_classes([AllowAny])
def create_contrat(request):
    data = request.data
    id_devis = data['id_devis']
    id_client = data['id_client']
    type_devis = data['type_devis']
    if type_devis == "mai":
        try:
            devis = Devis_Maison.objects.get(id=id_devis)
        except Devis_Maison.DoesNotExist:
            devis = None
    if type_devis == "app":
        try:
            devis = Devis_Appartement.objects.get(id=id_devis)
        except Devis_Appartement.DoesNotExist:
            devis = None
    if type_devis == "imm":
        try:
            devis = Devis_Immeuble.objects.get(id=id_devis)
        except Devis_Immeuble.DoesNotExist:
            devis = None


@api_view(["POST"])
@permission_classes([AllowAny])
def client_all_contrat(request):
    data = request.data
    contrats = get_all_client_contrat(data)
    return Response(contrats)


@api_view(["GET"])
@permission_classes([AllowAny])
def get_all_contrat(request):
    contrats = get_all_contrats()
    return Response(contrats)
