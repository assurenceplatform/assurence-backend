# Generated by Django 4.0.2 on 2022-03-10 00:57

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import multiselectfield.db.fields
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('prospect', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Devis_Maison',
            fields=[
                ('id', models.UUIDField(blank=True, default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('adresse', models.CharField(blank=True, max_length=250, null=True, verbose_name="adresse de l'immeuble")),
                ('complement', models.CharField(blank=True, max_length=250, null=True, verbose_name="complement d'adresse ")),
                ('email_prospect', models.EmailField(blank=True, max_length=254, null=True, verbose_name='email de prospect')),
                ('Vérnada', models.BooleanField(default=False, verbose_name='est ce il y a veranda')),
                ('Cheminée', models.BooleanField(default=False, verbose_name='est ce il y a Cheminée')),
                ('Piscine', models.BooleanField(default=False, verbose_name='est ce il y a Piscine')),
                ('type_residence', models.CharField(blank=True, choices=[('principale', 'principale'), ('secondaire', 'secondaire')], max_length=255, null=True, verbose_name='Type_residence')),
                ('type_propriete', models.CharField(blank=True, choices=[('Locataire', 'Locataire'), ('proprietaire', 'proprietaire'), ('proprietaire non occupant', 'proprietaire non occupant')], max_length=255, null=True, verbose_name='Type_propriete')),
                ('interraction', models.BooleanField(blank=True, null=True, verbose_name='permission de contrat')),
                ('Surface', models.IntegerField(blank=True, default=25, null=True, validators=[django.core.validators.MinValueValidator(25), django.core.validators.MaxValueValidator(1000)], verbose_name='surface habitat')),
                ('nbrChambre', models.IntegerField(blank=True, default=0, null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(25)], verbose_name='Nombre des Chambres')),
                ('dependance', models.BooleanField(blank=True, default=False, null=True, verbose_name='depandance de residance')),
                ('surface_depandance', models.CharField(blank=True, choices=[('plus de 100 m ', 'plus de 100 m'), ('Moin de 21 m', 'Moin de 21 m'), ('entre 21 m et 100 m ', 'entre 21 m et 100 m')], default=0, max_length=255, null=True, verbose_name='surface de depandance')),
                ('periode_construction', models.CharField(blank=True, choices=[('Avant 1960', 'Avant 1960'), ('Entre 1960 et 1980', 'Entre 1960 et 1980'), ('Entre 1981 et 2000 ', 'Entre 1981 et 2000'), ('Après 2001', 'Après 2001')], max_length=80, null=True, verbose_name='annee de contruction de maison')),
                ('garanties', multiselectfield.db.fields.MultiSelectField(blank=True, choices=[('item_key1', 'Item title 1.1'), ('item_key2', 'Item title 1.2'), ('item_key3', 'Item title 1.3'), ('item_key4', 'Item title 1.4'), ('item_key5', 'Item title 1.5')], max_length=49, null=True)),
                ('type_resiliation', models.CharField(blank=True, choices=[('demenage', 'demenage'), ('changer habitat', 'changer habitat')], max_length=255, null=True, verbose_name='type de resiliation')),
                ('nbr_sinistre', models.CharField(blank=True, choices=[('Aucun', 'Aucun'), ('un', 'un'), ('deux', 'deux'), ('Trois ou plus', 'Trois ou plus')], max_length=255, null=True, verbose_name='nombre des sinistre')),
                ('resiliation', models.BooleanField(blank=True, null=True, verbose_name='Besoin resiliation')),
                ('periode_resiliation', models.CharField(blank=True, choices=[('plus de 12 mois ', 'plus de 12 mois'), ('Moin de 12 mois', 'Moin de 12 mois')], max_length=255, null=True, verbose_name='periode de resiliation')),
                ('date_resiliation', models.CharField(blank=True, max_length=15, null=True, verbose_name='mois de resiliation')),
                ('Tarification', models.FloatField(blank=True, null=True, verbose_name='Tarification devis')),
                ('franchise', models.FloatField(blank=True, null=True, verbose_name='Franchise sur devis')),
                ('prospect', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='prospect.prospect', verbose_name='le proprietary de devis')),
            ],
        ),
        migrations.CreateModel(
            name='Devis_Immeuble',
            fields=[
                ('id', models.UUIDField(blank=True, default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('adresse', models.CharField(blank=True, max_length=250, null=True, verbose_name="adresse de l'immeuble")),
                ('complement', models.CharField(blank=True, max_length=250, null=True, verbose_name="complement d'adresse ")),
                ('surface_immeuble', models.IntegerField(blank=True, default=0, null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(25)], verbose_name='Surface Immeuble')),
                ('nombre_lots', models.IntegerField(blank=True, default=0, null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(25)], verbose_name='Nombre de lots')),
                ('Niveau_immeuble', models.CharField(blank=True, choices=[('5 niveau ou plus ', '5 niveau ou plus '), ('Entre 6 et 8 niveaux', 'Entre 6 et 8 niveaux'), ('9 niveaux ou plus', '9 niveaux ou plus')], max_length=255, null=True, verbose_name="niveau d'etage de l immeuble ")),
                ('Niveau_sous_sol', models.CharField(blank=True, choices=[('Aucun ', 'Aucun'), ('Moins de 3 Niveaux', 'Moins de 3 Niveaux'), ('3 niveauc ou plus', '3 niveauc ou plus')], max_length=255, null=True, verbose_name='niveau sous sol de l immeuble ')),
                ('Parking', models.CharField(blank=True, choices=[('Pad de parking', 'Pad de parking'), ('Place de parking', 'Place de parking'), ('Place de parking et box', 'Place de parking et box'), ('Box fermé', 'Box fermé')], max_length=255, null=True, verbose_name='Type de parking')),
                ('type_propriete', models.CharField(blank=True, choices=[('coproprieté', 'coproprieté'), ('monoproprieté', ''), ('HLM', 'HLM')], max_length=255, null=True, verbose_name='Type_propriete')),
                ('type_copropriete', models.CharField(blank=True, choices=[('verticale', 'verticale'), ('horizontal', 'horizontal')], max_length=255, null=True, verbose_name='Type_copropriete')),
                ('type_batiment', models.CharField(blank=True, choices=[('Standard', 'Standard'), ('Standing', 'Standing')], max_length=255, null=True, verbose_name='Type_batiment')),
                ('periode_construction', models.CharField(blank=True, choices=[('Avant 1960', 'Avant 1960'), ('Entre 1960 et 1980', 'Entre 1960 et 1980'), ('Entre 1981 et 2000 ', 'Entre 1981 et 2000'), ('Après 2001', 'Après 2001')], max_length=80, null=True, verbose_name="annee de contruction de l'immeuble")),
                ('installation', multiselectfield.db.fields.MultiSelectField(blank=True, choices=[('Aucun', 'Aucun'), ('Réseau de sitribution de gaz', 'Réseau de sitribution de gaz'), ('Rien à signaler', 'Rien à signaler')], max_length=50, null=True, verbose_name='installations')),
                ('traveaux', multiselectfield.db.fields.MultiSelectField(blank=True, choices=[('Toiture', 'Toiture'), ('Plombier', 'Plombier'), ('Facade', 'Facade')], max_length=23, null=True, verbose_name='Les Traveau réalisé')),
                ('usage_immeuble', multiselectfield.db.fields.MultiSelectField(blank=True, choices=[('Habitaion', 'Habitaion'), ('Activité commerciale ou professionnel', 'Activité commerciale ou professionnel')], max_length=47, null=True)),
                ('activite_commerciale', multiselectfield.db.fields.MultiSelectField(blank=True, choices=[('Aucun', 'Aucun'), ('un', 'un'), ('deux', 'deux'), ('Trois ou plus', 'Trois ou plus')], max_length=355, null=True)),
                ('occupation', models.CharField(blank=True, choices=[('Occupé pour plus de 75% de sa surface ', 'Occupé pour plus de 75% de sa surface '), ('Occupé entre 50% et 75% de sa surface ', 'Occupé entre 50% et 75% de sa surface '), ('Occupé pour moins de 50% de sa surface', 'Occupé pour moins de 50% de sa surface'), ('Non, inoccupé et/ou arreté de péril ', 'Non, inoccupé et/ou arreté de péril ')], max_length=255, null=True, verbose_name="Taux d'ccupation")),
                ('taux_proprietaire', models.BooleanField(blank=True, null=True, verbose_name='plus de 50% des proprietaire sont occupant')),
                ('nbr_sinistre', models.CharField(blank=True, choices=[('Aucun', 'Aucun'), ('un', 'un'), ('deux', 'deux'), ('Trois ou plus', 'Trois ou plus')], max_length=255, null=True, verbose_name='nombre des sinistre')),
                ('type_entreprise', models.CharField(blank=True, choices=[('Un particulier ', 'Un particulier '), ('Une SCI', 'Une SCI'), ('Une ASL / une AFULL', 'Une ASL / une AFULL'), ('Un syndic bénévole', 'Un syndic bénévole'), ('Un syndic professionnel', 'Un syndic professionnel'), ('un membre du conseil syndical', 'un membre du conseil syndical'), ('Une SAS / une SARL ', 'Une SAS / une SARL ')], max_length=255, null=True, verbose_name='nombre des sinistre')),
                ('date_assemblee', models.CharField(max_length=15, null=True, verbose_name="la date de l'assemblé generale")),
                ('ancien_assurence', models.BooleanField(default=False, verbose_name='avez vous ancien api')),
                ('resiliation', models.BooleanField(blank=True, null=True, verbose_name='Besoin resiliation')),
                ('type_residence', models.CharField(blank=True, choices=[('principale', 'principale'), ('secondaire', 'secondaire')], max_length=255, null=True, verbose_name='Type_residence')),
                ('periode_prochaine_resiliation', models.DateField(blank=True, null=True, verbose_name='date de prochaine resiliation')),
                ('garanties', multiselectfield.db.fields.MultiSelectField(blank=True, choices=[('item_key1', 'Item title 1.1'), ('item_key2', 'Item title 1.2'), ('item_key3', 'Item title 1.3'), ('item_key4', 'Item title 1.4'), ('item_key5', 'Item title 1.5')], max_length=49, null=True)),
                ('Tarification', models.FloatField(blank=True, null=True, verbose_name='Tarification devis')),
                ('franchise', models.FloatField(blank=True, null=True, verbose_name='Franchise sur devis')),
                ('email_prospect', models.EmailField(blank=True, max_length=254, null=True, verbose_name='email de prospect')),
                ('prospect', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='prospect.prospect', verbose_name='le proprietary de devis')),
            ],
        ),
        migrations.CreateModel(
            name='Devis_Appartement',
            fields=[
                ('id', models.UUIDField(blank=True, default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('adresse', models.CharField(blank=True, max_length=250, null=True, verbose_name="adresse de l'immeuble")),
                ('complement', models.CharField(blank=True, max_length=250, null=True, verbose_name="complement d'adresse ")),
                ('email_prospect', models.EmailField(blank=True, max_length=254, null=True, verbose_name='email de prospect')),
                ('ancien_assurence', models.BooleanField(default=False, verbose_name='avez vous ancien api')),
                ('type_residence', models.CharField(blank=True, choices=[('principale', 'principale'), ('secondaire', 'secondaire')], max_length=255, null=True, verbose_name='Type_residence')),
                ('type_propriete', models.CharField(blank=True, choices=[('Locataire', 'Locataire'), ('proprietaire', 'proprietaire'), ('proprietaire non occupant', 'proprietaire non occupant')], max_length=255, null=True, verbose_name='Type_propriete')),
                ('interraction', models.BooleanField(blank=True, null=True, verbose_name='permission de contrat')),
                ('Surface', models.IntegerField(blank=True, default=25, null=True, validators=[django.core.validators.MinValueValidator(25), django.core.validators.MaxValueValidator(1000)], verbose_name='surface habitat')),
                ('nbrChambre', models.IntegerField(blank=True, default=0, null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(25)], verbose_name='Nombre des Chambres')),
                ('dependance', models.BooleanField(blank=True, default=False, null=True, verbose_name='depandance de residance')),
                ('surface_depandance', models.CharField(blank=True, choices=[('plus de 100 m ', 'plus de 100 m'), ('Moin de 21 m', 'Moin de 21 m'), ('entre 21 m et 100 m ', 'entre 21 m et 100 m')], default=0, max_length=255, null=True, verbose_name='surface de depandance')),
                ('garanties', multiselectfield.db.fields.MultiSelectField(blank=True, choices=[('item_key1', 'Item title 1.1'), ('item_key2', 'Item title 1.2'), ('item_key3', 'Item title 1.3'), ('item_key4', 'Item title 1.4'), ('item_key5', 'Item title 1.5')], max_length=49, null=True)),
                ('type_resiliation', models.CharField(blank=True, choices=[('demenage', 'demenage'), ('changer habitat', 'changer habitat')], max_length=255, null=True, verbose_name='type de resiliation')),
                ('nbr_resiliation', models.CharField(blank=True, choices=[('Aucun', 'Aucun'), ('un', 'un'), ('deux', 'deux'), ('Trois ou plus', 'Trois ou plus')], max_length=255, null=True, verbose_name='nombre des resiliation')),
                ('nbr_sinistre', models.CharField(blank=True, max_length=255, null=True, verbose_name='nombre des sinistre')),
                ('resiliation', models.BooleanField(blank=True, null=True, verbose_name='Besoin resiliation')),
                ('date_resiliation', models.CharField(blank=True, max_length=15, null=True, verbose_name='mois de resiliation')),
                ('periode_resiliation', models.CharField(blank=True, choices=[('plus de 12 mois ', 'plus de 12 mois'), ('Moin de 12 mois', 'Moin de 12 mois')], max_length=255, null=True, verbose_name='periode de resiliation')),
                ('mois_resiliation', models.CharField(blank=True, max_length=15, null=True, verbose_name='mois de resiliation')),
                ('periode_prochaine_resiliation', models.DateField(blank=True, null=True, verbose_name='date de prochaine resiliation')),
                ('Tarification', models.FloatField(blank=True, null=True, verbose_name='Tarification devis')),
                ('franchise', models.FloatField(blank=True, null=True, verbose_name='Franchise sur devis')),
                ('prospect', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='prospect.prospect', verbose_name='le proprietary de devis')),
            ],
        ),
    ]
