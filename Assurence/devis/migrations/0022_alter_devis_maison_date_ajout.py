# Generated by Django 4.0.2 on 2022-03-31 12:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('devis', '0021_alter_devis_immeuble_date_ajout_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='devis_maison',
            name='date_ajout',
            field=models.DateField(auto_now_add=True),
        ),
    ]
