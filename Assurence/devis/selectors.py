from django.db.models.query import QuerySet
from django.apps import apps
from .models import *
from client.selectors import verify_client
from .serialisers import *

Prospect = apps.get_model('prospect', 'Prospect')
Client = apps.get_model('client', 'Client')


def get_Prospects() -> QuerySet[Prospect]:
    try:
        prospects = Prospect.objects.all()
    except Prospect.DoesNotExist:
        prospects = None
    return prospects


def get_Prospect(id) -> Prospect:
    try:
        client = Prospect.objects.get(id=id)
    except Prospect.DoesNotExist:
        client = None
    return client


def get_client(id) -> Client:
    try:
        client = Client.objects.get(id=id)
    except Client.DoesNotExist:
        client = None
    return client


def get_all_devis_client(id):
    verif = verify_client(id)
    if not verif:
        return None
    client_devis = []
    try:
        devis_maisons = Devis_Maison.objects.filter(client=id)
        devis_data = DevisMaisonSerializers(devis_maisons, many=True)
        client_devis.extend(devis_data.data)
    except Devis_Maison.DoesNotExist:
        devis_maison = None
    try:
        devis_immeubles = Devis_Immeuble.objects.filter(client=id)
        devis_data = DevisImmeubleSerializers(devis_immeubles, many=True)
        client_devis.extend(devis_data.data)
    except Devis_Immeuble.DoesNotExist:
        devis_immeubles = None
    try:
        devis_appartements = Devis_Appartement.objects.filter(client=id)
        devis_data = DevisAppartmentSerializers(devis_appartements, many=True)
        client_devis.extend(devis_data.data)
    except Devis_Appartement.DoesNotExist:
        devis_appartements = None
    if client_devis:
        return client_devis
    return None


def get_devis_from_id_ser(id) -> object:
    try:
        devis = Devis_Maison.objects.get(id=id)
        serializer = DevisMaisonSerializers(devis)
    except Devis_Maison.DoesNotExist:
        pass
    try:
        devis = Devis_Immeuble.objects.get(id=id)
        serializer = DevisImmeubleSerializers(devis)
    except Devis_Immeuble.DoesNotExist:
        pass
    try:
        devis = Devis_Appartement.objects.get(id=id)
        serializer = DevisAppartmentSerializers(devis)
    except Devis_Appartement.DoesNotExist:
        pass
    if serializer:
        return serializer.data
    return None


def get_devis_from_id(id) -> object:
    devis = None
    try:
        devis = Devis_Maison.objects.get(id=id)
    except Devis_Maison.DoesNotExist:
        pass
    try:
        devis = Devis_Immeuble.objects.get(id=id)
    except Devis_Immeuble.DoesNotExist:
        pass
    try:
        devis = Devis_Appartement.objects.get(id=id)
    except Devis_Appartement.DoesNotExist:
        pass
    if devis:
        return devis
    return None
