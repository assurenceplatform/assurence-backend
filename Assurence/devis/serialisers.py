from rest_framework.serializers import ModelSerializer
from .models import *


class DevisMaisonSerializers(ModelSerializer):
    class Meta:
        model = Devis_Maison
        fields = '__all__'


class DevisMaisonUpdate_Serializer(ModelSerializer):
    class Meta:
        model = Devis_Maison
        fields = ["id", "type_habitat", "adresse", "complement", "email_prospect", "Vérnada", "Cheminée", "Piscine",
                  "type_residence",
                  "type_propriete", "interraction", "Surface", "nbrChambre", "dependance", "surface_depandance",
                  "periode_construction", "garanties",
                  "type_resiliation", "nbr_sinistre", "resiliation", "periode_resiliation", "date_resiliation",
                  "Tarification", "franchise", "prospect", "client"]


class DevisAppartmentSerializers(ModelSerializer):
    class Meta:
        model = Devis_Appartement
        fields = '__all__'


class DevisImmeubleSerializers(ModelSerializer):
    class Meta:
        model = Devis_Immeuble
        fields = '__all__'
