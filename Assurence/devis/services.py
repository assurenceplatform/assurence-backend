from django.db.models.query import QuerySet
from django.apps import apps
from .serialisers import *
from .selectors import *

Prospect = apps.get_model('prospect', 'Prospect')
Devis_Maison = apps.get_model('devis', 'Devis_Maison')
Devis_Appartement = apps.get_model('devis', 'Devis_Appartement')
Devis_Immeuble = apps.get_model('devis', 'Devis_Immeuble')


def verif_Prospect(email) -> bool:
    if Prospect.objects.filter(adresse_email=email).exists():
        return True
    return False


def create_Prospect(data) -> object:
    adresse_email = data['adresse_email']
    if verif_Prospect(adresse_email):
        return {"error": "email already exists !! "}
    prospect = Prospect(
        id=data["id"],
        nom=data['nom'],
        prenom=data['prenom'],
        adresse_email=data['adresse_email'],
        adress=data["adresse"])
    prospect.save()
    return prospect


def create_devis_maison(data):
    id=None
    client = None
    if  "id" in data.keys():
        id = data["id"]
    if id:
        client = get_client(id)
        prospect = client.prospect
    else:
        prospect = create_Prospect(data["prospect"])
    if isinstance(prospect, Prospect):
        devis_maison = Devis_Maison.objects.create(
            prospect=prospect,
            adresse=data['adresse'],
            complement=data['complement'],
            email_prospect=data['email_prospect'],
            type_residence=data['type_residence'],
            Vérnada=data["Veranda"],
            Cheminée=data["Cheminée"],
            Piscine=data["Piscine"],
            type_propriete=data['type_propriete'],
            interraction=data['interraction'],
            Surface=data['Surface'],
            nbrChambre=data['nbrChambre'],
            dependance=data['dependance'],
            surface_depandance=data["surface_depandance"],
            periode_construction=data['periode_construction'],
            type_resiliation=data['type_resiliation'],
            nbr_sinistre=data['nbr_sinistre'],
            resiliation=data['resiliation'],
            periode_resiliation=data['periode_resiliation'],
            date_resiliation=data['date_resiliation'],
            Tarification=data['Tarification'],
            franchise=data['franchise'],
            client=client)
        devis_maison.save()
        return devis_maison
    return None


def create_devis_apartment(data):
    id = None
    client = None
    if "id" in data.keys():
        id = data["id"]
    if id:
        client = get_client(id)
        prospect = client.prospect
    else:
        prospect = create_Prospect(data["prospect"])
    if isinstance(prospect, Prospect):
        devis_apartment = Devis_Appartement.objects.create(
            prospect=prospect,
            adresse=data['adresse'],
            complement=data['complement'],
            email_prospect=data['email_prospect'],
            type_residence=data['type_residence'],
            type_propriete=data['type_propriete'],
            interraction=data['interraction'],
            Surface=data['Surface'],
            etage_appartement=data['etage_appartement'],
            nbrChambre=data['nbrChambre'],
            dependance=data['dependance'],
            surface_depandance=data["surface_depandance"],
            type_resiliation=data['type_resiliation'],
            nbr_sinistre=data['nbr_sinistre'],
            resiliation=data['resiliation'],
            periode_resiliation=data['periode_resiliation'],
            date_resiliation=data['date_resiliation'],
            Tarification=data['Tarification'],
            franchise=data['franchise'],
            client=client)
        devis_apartment.save()
        return devis_apartment
    return None


def create_devis_immeuble(data):
    id = None
    client = None
    if "id" in data.keys():
        id = data["id"]
    if id is not None:
        client = get_client(id)
        prospect = client.prospect
    else:
        prospect = create_Prospect(data["prospect"])
    if isinstance(prospect, Prospect):
        devis_immeuble = Devis_Immeuble.objects.create(
            prospect=prospect,
            adresse=data['adresse'],
            complement=data['complement'],
            surface_immeuble=data['surface_immeuble'],
            nombre_lots=data['nombre_lots'],
            Niveau_immeuble=data['Niveau_immeuble'],
            Niveau_sous_sol=data['Niveau_sous_sol'],
            Parking=data['Parking'],
            type_propriete=data['type_propriete'],
            type_copropriete=data['type_copropriete'],
            type_batiment=data['type_batiment'],
            periode_construction=data['periode_construction'],
            installation=data['installation'],
            traveaux=data['traveaux'],
            usage_immeuble=data['usage_immeuble'],
            activite_commerciale=data['activite_commerciale'],
            occupation=data['occupation'],
            taux_proprietaire=data['taux_proprietaire'],
            nbr_sinistre=data['nbr_sinistre'],
            type_entreprise=data['type_entreprise'],
            date_assemblee=data['date_assemblee'],
            ancien_assurence=data['ancien_assurence'],
            resiliation=data['resiliation'],
            Tarification=data['Tarification'],
            franchise=data['franchise'],
            email_prospect=data['email_prospect'],
            client=client
        )
        devis_immeuble.save()
        return devis_immeuble
    return None


def delete_devis_service(data) -> bool:
    type_devis = data['type']
    id = data['id']
    devis = None
    if type_devis == "Appartement":
        try:
            devis = Devis_Appartement.objects.get(id=id)
        except Devis_Appartement.DoesNotExist:
            pass

    if type_devis == "Maison":
        try:
            devis = Devis_Maison.objects.get(id=id)
        except Devis_Maison.DoesNotExist:
            pass

    if type_devis == "Immeuble":
        try:
            devis = Devis_Immeuble.objects.get(id=id)
        except Devis_Immeuble.DoesNotExist:
            pass
    if devis:
        devis.delete()
        return True
    return False


def update_devis_delete(data) -> object:
    id_devis = data['id_devis']
    type_devis = data['type_habitat']
    devis = get_devis_from_id(id_devis)
    if not devis:
        return {"error": "on Peut pas trouver cette devis"}
    devis.delete()
    if type_devis == "Appartement":
        devis = create_devis_apartment(data)
        serializer = DevisAppartmentSerializers(devis, many=False)
    if type_devis == "Maison":
        devis = create_devis_maison(data)
        serializer = DevisMaisonSerializers(devis, many=False)
    if type_devis == "Immeuble":
        devis = create_devis_immeuble(data)
        serializer = DevisImmeubleSerializers(devis, many=False)
    return serializer.data
