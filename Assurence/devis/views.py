from django.http import HttpResponse
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework import status
from rest_framework.response import Response

from .serialisers import *
from .models import *
from .services import *
from .selectors import *
from django.http import JsonResponse

import time


# Create your views here.


@api_view(["GET"])
@permission_classes([AllowAny])
def get_devis_maison(request):
    try:
        Devis_Maisons = Devis_Maison.objects.all()
    except Devis_Maison.DoesNotExist:
        Devis_Maisons = None
    serializer = DevisMaisonSerializers(Devis_Maisons, many=True)
    return Response(serializer.data)


@api_view(["POST"])
@permission_classes([AllowAny])
def save_devis_maison(request):
    data = request.data
    devis_maison = create_devis_maison(data)
    serializer = DevisMaisonSerializers(devis_maison)
    print(serializer.data)
    return Response(serializer.data)


@api_view(["GET"])
@permission_classes([AllowAny])
def get_devis_apartement(request):
    try:
        devis_appartments = Devis_Appartement.objects.all()
    except devis_appartments.DoesNotExist:
        devis_appartments = None
    serializer = DevisAppartmentSerializers(devis_appartments, many=True)
    return Response(serializer.data)


@api_view(["POST"])
@permission_classes([AllowAny])
def save_devis_apartment(request):
    data = request.data
    devis_apartment = create_devis_apartment(data)
    serializer = DevisAppartmentSerializers(devis_apartment, many=False)
    return Response(serializer.data)


@api_view(['GET'])
@permission_classes([AllowAny])
def get_devis_immeuble(request):
    try:
        devis_Immeuble = Devis_Immeuble.objects.all()
    except Devis_Immeuble.DoesNotExist:
        devis_Immeuble = None
    serializer = DevisMaisonSerializers(devis_Immeuble, many=True)
    return Response(serializer.data)


@api_view(["POST"])
@permission_classes([AllowAny])
def save_devis_immeuble(request):
    data = request.data
    devis_immeuble = create_devis_immeuble(data)
    serializer = DevisImmeubleSerializers(devis_immeuble, many=False)
    return Response(serializer.data)


@api_view(["POST"])
@permission_classes([AllowAny])
def delete_devis(request):
    data = request.data
    res = delete_devis_service(data)
    if res:
        return Response({"success", "devis is deleted successefully !! "})
    return Response({"error", "i can t find the devis y'are trying to delete !! "})


@api_view(["POST"])
@permission_classes([AllowAny])
def get_all_client_devis(request):
    id = request.data['id']
    list_devi_client = get_all_devis_client(id=id)
    return Response(list_devi_client)


@api_view(["POST"])
@permission_classes([AllowAny])
def get_devis_by_id(request):
    data = request.data
    res = get_devis_from_id_ser(data["id"])
    if res:
        return Response(res)
    return Response({"error": "there is an error occured !!! "})


@api_view(["POST"])
@permission_classes([AllowAny])
def delete_devis_by_id(request):
    data = request.data
    devis = get_devis_from_id(data['id'])
    if devis:
        devis.delete()
        return Response({"success": "Devis suprimer avec succés !"})
    return Response({"Error": "Devis n'existe pas"})


@api_view(["POST"])
@permission_classes([AllowAny])
def update_devis(request):
    data = request.data
    devis = update_devis_delete(data)
    if devis:
        return Response(devis, status=status.HTTP_200_OK)
    return Response(devis, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@permission_classes([AllowAny])
def update_devis_assurence(request):
    data = request.data

    id = data['id']
    print(data["personnalizer"])
    devis = get_devis_from_id(id)
    if devis:
        devis.franchise = data['personnalizer']["franchise"]
        devis.mobilier = data['personnalizer']['mobilier']
        devis.objet_valeur = data['personnalizer']['objet_valeur']
        devis.conjoint = data['beneficiaire']['conjoint']
        devis.enfant = data['beneficiaire']['enfant']
        devis.proches_collocataire = data['beneficiaire']['proches_collocataire']
        devis.assurence_scholaire = data['option']['assurence_scholaire']
        devis.domage_electrique = data['option']['domage_electrique']
        devis.valeur_neuf = data['option']['valeur_neuf']
        devis.save()
        print(devis)
        return Response({"success": "devis updated successefuly !"})
    return Response({"error": "devis introuvable !! "}, status=status.HTTP_200_OK)
