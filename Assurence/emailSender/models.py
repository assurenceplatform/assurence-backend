from django.db import models
from uuid import uuid4
from client.models import Client
from prospect.models import Prospect

# Create your models here.

class Mail(models.Model):
    mailObject = models.CharField(max_length=150, null=True, blank=True)
    mailBody = models.TextField(max_length=1500, null=True, blank=True)
    date_ajout = models.DateField(auto_now_add=True, null=True, blank=True)
    personnalisé = models.BooleanField(default=True, null=True, blank=True)
    clients = models.ManyToManyField(Client, null=True, blank=True)
    prospects = models.ManyToManyField(Prospect, null=True, blank=True)

    def __str__(self):
        return str(self.id) + " " + self.mailObject



