from .models import *
from django.apps import apps
from .serialisers import *

Client = apps.get_model('client', 'Client')

def get_Client(id):
    try:
        client = Client.objects.get(id=id)
    except Client.DoesNotExist:
        client = None
    return client