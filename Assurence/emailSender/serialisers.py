from rest_framework.serializers import ModelSerializer
from .models import *


class EmailSerializer(ModelSerializer):
    class Meta:
        model = Mail
        fields = '__all__'
