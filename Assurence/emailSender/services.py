from .models import *
from django.core.mail import send_mail, BadHeaderError
from django.conf import settings


def send_mail_perso(data):
    sujet = data['sujet']
    mail_body = data['mail_body']
    client_email =[]
    client_email.append(data['client'])
    try:
        send_mail(
            sujet,
            mail_body,
            'jassem.elwaar@gmail.com',
            client_email)
    except BadHeaderError:
        return False
    return True


