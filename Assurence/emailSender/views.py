from django.shortcuts import render
from rest_framework.permissions import AllowAny
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from contrat.models import Contrat
from contrat.serializer import ContratSerializers
from prospect.models import Prospect
from .services import *
from .selectors import *
from rest_framework import status
from prospect.selectors import get_Prospect

from django.contrib.auth import get_user_model

UserAccount = get_user_model()
from rest_framework.response import Response
from .models import Mail
from uuid import uuid4


@api_view(['POST'])
@permission_classes([AllowAny])
def envoyer_mail_personnalizer(request):
    data = request.data
    type = data['contrat']['type']
    status = data['contrat']['status']
    liste_des_contrat = Contrat.objects.filter(status__in=status, contrat_type__in=type)
    if liste_des_contrat:
        list_client_emails = [i.client.email for i in liste_des_contrat]
        list_client = [i.client for i in liste_des_contrat]
        data = {"client": list_client_emails, "sujet": data['sujet'], "mail_body": data['mail_body']}

        res = send_mail_perso(data)
        if res:
            mails = Mail.objects.create(mailObject=data['sujet'], mailBody=data['mail_body'], personnalisé=True)
            mails.clients.set(list_client)
            mails.save()
            return Response({"success": "les mail  ont été envoyés"})
        return Response({"error": "mail error"})
    return Response({"errur": "il y a pas des contrat avec ses condition"})


@api_view(['POST'])
@permission_classes([AllowAny])
def envoyer_mail(request):
    data = request.data
    client = get_Client(data['client'])
    print("this is client" ,  client)
    clients = []
    clients.append(client)
    if client:
        data = {"client": client.email, "sujet": data['sujet'], "mail_body": data['mail_body']}
        res = send_mail_perso(data)
        if res:
            mails = Mail.objects.create(mailObject=data['sujet'], mailBody=data['mail_body'], personnalisé=True)
            mails.clients.set(clients)
            mails.save()
            return Response({"success": "les mail  ont été envoyés"})
        return Response({"erreur": "mail error"})
    return Response({"erreur": "client introuvable !  "})
