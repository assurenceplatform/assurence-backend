from django.db import models
from client.models import Client
import uuid


# Create your models here.

class Notification(models.Model):
    id = models.CharField(primary_key=True, default=uuid.uuid4, blank=True, editable=False, max_length=20)
    clients = models.ManyToManyField(Client)
    sujet = models.CharField(max_length=255, verbose_name="le sujet de la plainte", null=True, blank=True)
    email = models.EmailField(verbose_name="Email de client qui pose le question", null=True, blank=True)
    body = models.TextField(max_length=2500, null=True, blank=True)
    date_ajout = models.DateField(auto_now_add=True)
    personnalisé = models.BooleanField(default=True, null=True, blank=True)
    opened = models.BooleanField(default=False)

    def __str__(self):
        return "plaintes" + str(self.id) + self.sujet
