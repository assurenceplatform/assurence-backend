from .models import *
from django.apps import apps

Client = apps.get_model('client', 'Client')


def get_Client(id) -> Client:
    try:
        client = Client.objects.get(id=id)
    except Client.DoesNotExist:
        client = None
    return client




def get_client_notification(id) -> object:
    try:
        notifications = Notification.objects.filter(client__id=id)
    except Notification.DoesNotExist:
        notifications = None
    if notifications:
        return notifications
    return None


def get_notification_by_id(id) -> object:
    try:
        notification = Notification.objects.get(id=id)
    except Notification.DoesNotExist:
        notification = None
    if notification:
        return notification
    return notification


def count_unoppened_notifications(id) -> int:
    new_notifications = 0
    try:
        notifications = Notification.objects.filter(client__id=id)
        new_notifications = notifications.filter(opened=False).count()
    except Notification.DoesNotExist:
        new_notifications = 0
    return new_notifications
