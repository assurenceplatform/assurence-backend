from rest_framework.serializers import ModelSerializer
from .models import *


class NotificationsSerializer(ModelSerializer):
    class Meta:
        model = Notification
        fields = '__all__'
