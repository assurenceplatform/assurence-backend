"""api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from .views import *

urlpatterns = [
    path('create', create_notification, name="create_notification"),
    path('get_all', get_all_notifications, name="get_notifications"),
    path('get_client_notifications/', get_client_notifications, name="get_notifications"),
    path('get_notification', get_notification, name="get_notification"),
    path('delete_notification', delete_notification, name="delete_notification"),
    path('count_notification', count_new_notification, name="new_notifications"),
    path('update_opened', update_opened_notification, name="update_opened"),
    path('create_custom_notification', create_notification_personnalizer, name="create_notification_personnalizer")
]
