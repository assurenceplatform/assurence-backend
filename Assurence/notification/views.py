from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework import status
from .serialisers import *
from contrat.models import Contrat
from .models import *
from .selectors import *
from .services import *


@api_view(["GET"])
@permission_classes([AllowAny])
def get_all_notifications(request):
    try:
        notifications = Notification.objects.all()
    except Notification.DoesNotExist:
        notifications = None
    serializers = NotificationsSerializer(notifications, many=True)
    return Response(serializers.data)


@api_view(["POST"])
@permission_classes([AllowAny])
def create_notification(request):
    data = request.data
    client = get_Client(data['id'])
    clients = []
    clients.append(client)
    if client:
        notification = Notification.objects.create(
            email=client.user.email,
            sujet=data["sujet"],
            body=data["body"])
        notification.client.set(clients)
        notification.save()
        serializer = NotificationsSerializer(notification, many=False)
        return Response(serializer.data)
    return Response({"error": "La notification ne peut pas etre enregistrer"}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@permission_classes([AllowAny])
def get_client_notifications(request):
    data = request.data
    print("this si id ", data)
    notifications = get_client_notification(data['id'])
    if notifications:
        serializer = NotificationsSerializer(notifications, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response({"error": "Ce client n'a pas des notifications ! "}, status=status.HTTP_204_NO_CONTENT)


@api_view(["GET"])
@permission_classes([AllowAny])
def get_notification(request):
    id = request.data['id']
    notification = get_notification_by_id(id)
    if notification:
        serializer = NotificationsSerializer(notification, many=False)
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response({"Error": "Notification n'existe pas"}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@permission_classes([AllowAny])
def delete_notification(request):
    print(request)
    id = request.data['id']
    print("thi", id)
    notification = get_notification_by_id(id)
    if notification:
        notification.delete()
        return Response({"Success": "cette notification a été supprimer !"}, status=status.HTTP_200_OK)
    return Response({"Error": "il ya pas une notification de cette id actualiser la page s'il vous plait ! "},
                    status=status.HTTP_400_BAD_REQUEST)


@api_view(["GET"])
@permission_classes([AllowAny])
def count_new_notification(request):
    id = request.data["id"]
    new_notifications = count_unoppened_notifications(id)
    return Response({"new_notifications": new_notifications}, status=status.HTTP_200_OK)


@api_view(["POST"])
@permission_classes([AllowAny])
def update_opened_notification(request):
    id = request.data["id"]
    notification = get_notification_by_id(id)
    print(notification.opened)
    if notification.opened:
        return Response({"Error": "notification already visited"}, status=status.HTTP_200_OK)
    notification.opened = True
    notification.save()
    serializer = NotificationsSerializer(notification, many=False)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([AllowAny])
def create_notification_personnalizer(request):
    data = request.data
    type = data['contrat']['type']
    status = data['contrat']['status']
    if type or status:
        liste_des_contrat = Contrat.objects.filter(status__in=status, contrat_type__in=type)
    else:
        liste_des_contrat = Contrat.objects.filter()
    if liste_des_contrat:
        list_client = [i.client for i in liste_des_contrat]
        mails = Notification.objects.create(sujet=data['sujet'], body=data['mail_body'], personnalisé=True)
        mails.clients.set(list_client)
        mails.save()
        return Response({"success": "les notifications ont été envoyés"})
    return Response({"errur": "il y a pas des contrat avec ses condition"})
