from django.db import models
import uuid
from client.models import Client


# Create your models here.

class Plainte(models.Model):
    id = models.CharField(primary_key=True, default=uuid.uuid4, blank=True, editable=False, max_length=20)
    client = models.ForeignKey(Client, on_delete=models.CASCADE , null=True ,blank=True ,  editable=True)
    sujet = models.CharField(max_length=255, verbose_name="le sujet de la plainte", null=True, blank=True)
    email = models.EmailField(verbose_name="Email de client qui pose le question", null=True, blank=True)
    body = models.TextField(max_length=2500, null=True, blank=True)
    answer = models.TextField(max_length=1000, null=True, blank=True)
    date_ajout = models.DateField(auto_now_add=True)
    udated_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "plaintes" + str(self.id) + self.sujet
