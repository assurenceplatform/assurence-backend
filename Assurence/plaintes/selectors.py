from .models import *
from django.apps import apps

Client = apps.get_model('client', 'Client')


def get_Client(id) -> Client:
    try:
        client = Client.objects.get(id=id)
    except Client.DoesNotExist:
        client = None
    return client


def get_client_plainte(id) -> object:
    try:
        client = get_Client(id)
        print(client)
        plaintes = Plainte.objects.filter(client=client)
    except Plainte.DoesNotExist:
        plaintes = None
    if plaintes:
        return plaintes
    return None


def get_plainte_by_id(id) -> object:
    try:
        plainte = Plainte.objects.get(id=id)
    except Plainte.DoesNotExist:
        plainte = None
    if plainte:
        return plainte
    return plainte
