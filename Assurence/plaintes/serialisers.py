from rest_framework.serializers import ModelSerializer
from .models import *


class PlaintesSerializer(ModelSerializer):
    class Meta:
        model = Plainte
        fields = '__all__'
