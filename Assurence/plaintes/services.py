from .models import *
from .selectors import *


def update_plainte_answer(data) -> object:
    id = data['id']
    answer = data['answer']
    plainte = get_plainte_by_id(id)
    if plainte:
        plainte.answer = answer
        plainte.save()
        return plainte
    return None


