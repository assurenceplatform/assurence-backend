"""api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from .views import *

urlpatterns = [
    path('create', save_plainte, name="create_plainte"),
    path('create_email_plainte', save_plainte_email, name="create_plainte_per_email"),
    path('get_all', get_all_plaintes, name="get_plaintes"),
    path('get_client_plaintes/', get_client_plaintes, name="get_plaintes"),
    path('get_plainte', get_plainte, name="get_plainte"),
    path('answer_plainte', answer_plainte, name="answer_plainte"),
    path('delete_plainte', delete_plainte, name="delete_plainte"),
]
