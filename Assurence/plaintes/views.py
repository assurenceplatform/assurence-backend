from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework import status
from .serialisers import *
from .models import *
from .selectors import *
from .services import *


@api_view(["GET"])
@permission_classes([AllowAny])
def get_all_plaintes(request):
    try:
        plaintes = Plainte.objects.all()
    except Plainte.DoesNotExist:
        plaintes = None
    serializers = PlaintesSerializer(plaintes, many=True)
    return Response(serializers.data)


@api_view(["POST"])
@permission_classes([AllowAny])
def save_plainte(request):
    data = request.data
    client = get_Client(data['id'])
    if client:
        plainte = Plainte.objects.create(
            email=client.user.email,
            client=client,
            sujet=data["sujet"],
            body=data["body"])
        plainte.save()
        serializer = PlaintesSerializer(plainte, many=False)
        return Response(serializer.data)
    return Response({"error": "La plainte ne peut pas etre enregistrer"}, status=status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
@permission_classes([AllowAny])
def save_plainte_email(request):
    data = request.data
    plainte = Plainte.objects.create(
        email=data['email'],
        sujet=data["sujet"],
        body=data["body"])
    plainte.save()
    serializer = PlaintesSerializer(plainte, many=False)
    return Response(serializer.data)



@api_view(["POST"])
@permission_classes([AllowAny])
def get_client_plaintes(request):
    data = request.data
    print("this si id ", data)
    plaintes = get_client_plainte(data['id'])
    if plaintes:
        serializer = PlaintesSerializer(plaintes, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response({"error": "Ce client n'a pas des plaintes ! "}, status=status.HTTP_204_NO_CONTENT)


@api_view(["GET"])
@permission_classes([AllowAny])
def get_plainte(request):
    id = request.data['id']
    plainte = get_plainte_by_id(id)
    if plainte:
        serializer = PlaintesSerializer(plainte, many=False)
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response({"Error": "Plainte n'existe pas"}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@permission_classes([AllowAny])
def answer_plainte(request):
    data = request.data
    plainte = update_plainte_answer(data)
    if plainte:
        serializer = PlaintesSerializer(plainte, many=False)
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response({"Error": "il ya pas une plainte de cette id  actualiser la page s'il vous plait"},
                    status=status.HTTP_400_BAD_REQUEST)


@api_view(["DELETE"])
@permission_classes([AllowAny])
def delete_plainte(request):
    id = request.data['id']
    plainte = get_plainte_by_id(id)
    if plainte:
        plainte.delete()
        return Response({"Success": "cette plainte a été supprimer !"}, status=status.HTTP_200_OK)
    return Response({"Error": "il ya pas une plainte de cette id actualiser la page s'il vous plait ! "},
                    status=status.HTTP_400_BAD_REQUEST)
