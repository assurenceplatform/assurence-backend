# Generated by Django 4.0.2 on 2022-03-10 00:57

import django.core.validators
from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Prospect',
            fields=[
                ('id', models.CharField(default=uuid.uuid4, editable=False, max_length=20, primary_key=True, serialize=False, unique=True)),
                ('nom', models.CharField(blank=True, max_length=30, null=True, verbose_name='Prénom')),
                ('prenom', models.CharField(blank=True, max_length=30, null=True, verbose_name='Nom')),
                ('adresse_email', models.EmailField(blank=True, max_length=254, null=True, verbose_name='Email')),
                ('adress', models.CharField(blank=True, max_length=255, null=True)),
                ('date_naissance', models.CharField(blank=True, max_length=55, null=True)),
                ('add_date', models.DateField(auto_now_add=True, verbose_name="Date d'ajout")),
                ('reminded', models.IntegerField(blank=True, default=0, null=True, validators=[django.core.validators.MinValueValidator(0)], verbose_name='remided times')),
            ],
        ),
    ]
