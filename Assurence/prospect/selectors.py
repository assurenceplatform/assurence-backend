from django.db.models.query import QuerySet
from django.apps import apps

Prospect = apps.get_model('prospect', 'Prospect')


def get_Prospects() -> QuerySet[Prospect]:
    try:
        prospects = Prospect.objects.all()
    except Prospect.DoesNotExist:
        prospects = None
    return prospects


def get_Prospect(id) -> Prospect:
    try:
        prospect = Prospect.objects.get(id=id)
    except Prospect.DoesNotExist:
        prospect = None
    return prospect
