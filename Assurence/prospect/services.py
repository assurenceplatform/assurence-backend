from django.db.models.query import QuerySet
from django.apps import apps

Prospect = apps.get_model('prospect', 'Prospect')


def verif_Prospect(email) -> bool:
    if Prospect.objects.filter(adresse_email=email).exists():
        return True
    return False


def create_Prospect(data) -> object:
    adresse_email = data['adresse_email']
    if verif_Prospect(adresse_email):
        return {"error": "email already exists !! "}
    prospect = Prospect(
        id=data["id"],
        nom=data['nom'],
        prenom=data['prenom'],
        adresse_email=adresse_email,
        adress=data["adress"],
        date_naissance=data['date_naissance'])
    prospect.save()
    return prospect
