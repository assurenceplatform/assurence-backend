from factory.django  import DjangoModelFactory
from factory import Faker
from  client.models import Client
from datetime import date


fake= Faker()

class Client_Factory(DjangoModelFactory):
    class Meta:
        model = Client

    nom = fake.name()
    prenom = fake.name()
    adresse_email = fake.email()
    adress = fake.adress()
    date_naissance = fake.date()
    add_date = date.today()
    reminded = fake.random()
