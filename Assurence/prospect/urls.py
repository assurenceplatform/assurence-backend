from django.contrib import admin
from django.urls import path
from .views import *

urlpatterns = [
    path('create', save_prospect ,  name="create_prospect"),
    path('get_all', get_all_prospect ,  name="get_prospects"),
    path('get_prospect', get_prospect ,  name="get_prospect"),
    path('update', update_prospect, name="update_prospect"),

]
