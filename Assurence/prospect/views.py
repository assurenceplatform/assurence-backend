from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from .models import *
from .serialisers import *
from .selectors import *
from .services import *


# Create your views here.


@api_view(['POST'])
@permission_classes([AllowAny])
def save_prospect(request):
    data = request.data
    prospect = create_Prospect(data)
    if isinstance(prospect, Prospect):
        serializer = ProspectSerializers(prospect, many=False)
        return Response(serializer.data)
    return Response({"Error": "There is duplication in the adresse email  please fix it !!"})


@api_view(["GET"])
@permission_classes([AllowAny])
def get_all_prospect(request):
    prospects = get_Prospects()
    if prospects:
        serializer = ProspectSerializers(prospects, many=True)
        return Response(serializer.data)
    return Response(prospects)


@api_view(["GET"])
@permission_classes([AllowAny])
def get_prospect(request):
    data = request.data
    id = data["id"]
    prospect = get_Prospect(id)
    if isinstance(prospect, Prospect):
        serializer = ProspectSerializers(prospect, many=False)
        return Response(serializer.data)
    return Response(prospect)


@api_view(["PUT"])
@permission_classes([AllowAny])
def update_prospect(request):
    data = request.data
    id = data['id']
    prospect = Prospect.objects.get(id=id)
    projec = ProspectSerializers(prospect, data=data)
    if projec.is_valid():
        projec.save()
    return Response(projec.data)
