from django.db import models
import uuid
from contrat.models import Contrat
from client.models import Client


# Create your models here.


class Sinistre(models.Model):
    status_sinistre = (("Envoyé", "Envoyé"), ("Valider", "Valider"), ("Réfusé", "Réfusé"))

    id = models.CharField(primary_key=True, default=uuid.uuid4, blank=True, editable=False, max_length=20)
    contrat = models.ForeignKey(Contrat, on_delete=models.CASCADE, null=True, blank=True, editable=True)
    sujet = models.CharField(max_length=255, verbose_name="le sujet de la plainte", null=True, blank=True)
    client = models.ForeignKey(Client, on_delete=models.CASCADE, null=True, blank=True, editable=True)
    body = models.TextField(max_length=2500, null=True, blank=True)
    status = models.CharField(max_length=35, null=True, blank=True, choices=status_sinistre)
    Sinistre_document = models.FileField(upload_to='static/Sinistre/origin/%Y/%m/%d/', default=None, blank=True,
                                         null=True)
    message_de_retour = models.TextField(max_length=2500, null=True, blank=True)
    explication_client = models.TextField(max_length=1500, null=True, blank=True)
    reponse_questionnaire = models.BooleanField(default=True, null=True, blank=True)
    reponse_questionnaire_psycologique = models.BooleanField(default=True, null=True, blank=True)
    date_ajout = models.DateField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.id) + " sinistre "












