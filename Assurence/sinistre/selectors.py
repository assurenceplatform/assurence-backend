from .models import *
from django.apps import apps
from .serialisers import *

Client = apps.get_model('client', 'Client')
Contrat = apps.get_model('contrat', 'Contrat')


def get_Client(id):
    try:
        client = Client.objects.get(id=id)
    except Client.DoesNotExist:
        client = None
    return client


def get_Contrat(id):
    try:
        contrat = Contrat.objects.get(id=id)
    except Contrat.DoesNotExist:
        contrat = None
    return contrat


def get_contrat_Sinistre(id) -> object:
    try:
        contrat = get_Contrat(id)
        if not isinstance(contrat, Contrat):
            return None
        sinistres = Sinistre.objects.filter(contrat=contrat)
    except Sinistre.DoesNotExist:
        sinistres = None
    if sinistres:
        ser = SinistreSerializer(sinistres, many=True)
        return ser.data
    return None


def get_client_Sinistres(id) -> object:
    try:
        client = get_Client(id)
        print(client)
        sinistres = Sinistre.objects.filter(client=client)
    except Sinistre.DoesNotExist:
        sinistres = None
    if sinistres:
        ser = SinistreSerializer(sinistres.data, many=True)
        return ser.data
    return None


def get_sinistre_by_id(id) -> object:
    try:
        plainte = Sinistre.objects.get(id=id)
    except Sinistre.DoesNotExist:
        plainte = None
    if plainte:
        return plainte
    return plainte
