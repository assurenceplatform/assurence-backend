from rest_framework.serializers import ModelSerializer
from .models import *


class SinistreSerializer(ModelSerializer):
    class Meta:
        model = Sinistre
        fields = '__all__'
