from .models import *
from .serialisers import *
from .selectors import *

'''def update_plainte_answer(data) -> object:
    id = data['id']
    answer = data['answer']
    plainte = get_plainte_by_id(id)
    if plainte:
        plainte.answer = answer
        plainte.save()
        return plainte
    return None
'''


def ajouter_Sinistre(data):
    print("this is data", data)
    sinistre = Sinistre.objects.create(
        contrat=data['contrat'],
        sujet=data["sujet"],
        client=data["client"],
        body=data['body'],
        status=data["status"],
        message_de_retour=data["message_de_retour"],
        explication_client=data["explication_client"],
        reponse_questionnaire=data['reponse_questionnaire'],
        reponse_questionnaire_psycologique=data["reponse_questionnaire_psycologique"]
    )
    sinistre.save()
    if isinstance(sinistre, Sinistre):
        ser = SinistreSerializer(sinistre, many=False)
        return ser.data
    return {"error": "errue lors de la creation de sinistre"}
