from django.shortcuts import render
from rest_framework.permissions import AllowAny
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from prospect.models import Prospect
from .services import *
from .selectors import *
from .serialisers import *
from rest_framework import status
from prospect.selectors import get_Prospect

from django.contrib.auth import get_user_model

UserAccount = get_user_model()
from rest_framework.response import Response
from uuid import uuid4



@api_view(['POST'])
@permission_classes([AllowAny])
def get_contrat_sinistre(request):
    data = request.data
    id = data["id"]
    print("this is data ", id)
    sinistre = get_contrat_Sinistre(id)
    return Response(sinistre)


@api_view(['GET'])
@permission_classes([AllowAny])
def get_client_sinistre(request):
    data = request.data
    print("this is data ",data)
    sinistre = get_client_Sinistres(data['id'])
    return Response(sinistre)


@api_view(['GET'])
@permission_classes([AllowAny])
def get_all_sinistres(request):
    sinsitres = Sinistre.objects.all()
    if sinsitres:
        ser = SinistreSerializer(sinsitres, many=True)
        return Response(ser.data, status=status.HTTP_200_OK)
    return Response({"error": "il y a aucun sinistres !! "})


@api_view(['POST'])
@permission_classes([AllowAny])
def ajouter_sinistre(request):
    data = request.data
    client = get_Client(data['client'])
    if not isinstance(client, Client):
        return Response({"error": "client est introuvable"})
    contrat = get_Contrat(data['contrat'])
    if not isinstance(contrat, Contrat):
        return Response({"error": "contrat est introuvable"})
    print("avant",data)
    data['contrat'] = contrat
    data['client'] = client
    sinistre = ajouter_Sinistre(data)
    return Response(sinistre)
