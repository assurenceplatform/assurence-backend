from factory.django import DjangoModelFactory
from user.models import UserAccount
from faker import Faker

fake = Faker()


class UserAccountFactory(DjangoModelFactory):
    class Meta:
        model = UserAccount

    email = fake.email()
    name = fake.name()
    is_active = 'True'
    is_staff = 'False'
